package LMS.controllers;

public class BookNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    BookNotFoundException(Long bookId) {
        super("Could not find Book " + bookId);
      }
    
}
