package LMS.controllers;

import java.util.List;

import LMS.domains.Customer;
import LMS.repositories.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    List<Customer> list() {
        return (List<Customer>) customerRepository.findAll();
    }

    @PostMapping("/customers")
    Customer newCustomer(@RequestBody Customer newCustomer) {     
        return customerRepository.save(newCustomer);
    }

    @GetMapping("/customers/{customerId}")
    Customer one(@PathVariable Long customerId) {

        return customerRepository.findById(customerId)
        .orElseThrow(() -> new CustomerNotFoundException(customerId));
    }

    @PutMapping("/customers/{customerId}")
    Customer replaceCustomer(@RequestBody Customer newCustomer, @PathVariable Long customerId) {

        return customerRepository.findById(customerId)
        .map(Customer -> {
            Customer.setCustomerNr(newCustomer.getCustomerNr());
            Customer.setCustomerName(newCustomer.getCustomerName());
            Customer.setBooks(newCustomer.getBooks());
            return customerRepository.save(Customer);
        })
        .orElseGet(() -> {
            return customerRepository.save(newCustomer);
        });
    }

    @DeleteMapping("/customers/{customerId}")
    void deleteCustomer(@PathVariable Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
