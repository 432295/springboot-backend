package LMS.controllers;

public class CustomerNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    CustomerNotFoundException(Long customerId) {
        super("Could not find Customer " + customerId);
      }
    
}
