package LMS.repositories;

import LMS.domains.Book;

//import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
    
    Book findByBookName(String bookName);  
    //List<Book> findAllByGenre(Book bookGenre);
}
