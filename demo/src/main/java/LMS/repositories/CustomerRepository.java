package LMS.repositories;

import LMS.domains.Customer;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findByCustomerName(String customerName);
}
