package LMS.domains;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Data
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;
    private String bookNr;
    private String bookName;

    // @ManyToOne
    // @JoinColumn(name="author_id", insertable = true, updatable = true, nullable = true)
    // private Author author;
    private String author;

    private String genre;
    private double price;
    private boolean available;

    @ManyToOne
    @JoinColumn(name="customer_id", insertable = true, updatable = true, nullable = true)
    private Customer customer;

    public Book() {
    }

    public Long getBookId() {
        return bookId;
    }

    public String getBookNr() {
        return bookNr;
    }

    public void setBookNr(String bookNr) {
        this.bookNr = bookNr;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    // @JsonBackReference
    // public Author getAuthor() {
    //     return author;
    // }

    // public void setAuthor(Author author) {
    //     this.author = author;
    // }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @JsonBackReference
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}